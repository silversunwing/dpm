package studio.crunchyiee.degreeprogrammemapper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0;
    Button btnGetStarted;
    Animation btnAnim;

    DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // ini views
        tabIndicator = findViewById(R.id.tab_indicator);

        // fill list screen - can be reproduced with loop and DB
        final List<ScreenItem> mList = new ArrayList<>();
        mList.add(new ScreenItem("Software Engineering", R.drawable.demo_img));
        mList.add(new ScreenItem("Networking", R.drawable.demo_img));
        mList.add(new ScreenItem("Database", R.drawable.demo_img));
        mList.add(new ScreenItem("Web Development", R.drawable.demo_img));

        dbHandler = new DBHandler(this, null, null, 1);

        //Inserting Data manually, this will be updated but use as prtotype for now
        //START OF MANUAL INSERT
        Course module = new Course("software", "COMP501", "IT Operations", 5, 15, "--", "Hardware component information.", 1);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP502", "Fundamentals of Programming and Problem Solving", 5, 15, "--", "Programming fundamentals.", 1);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO501", "Professional Practice", 5, 15, "--", "Business, Ethics and Morals.", 1);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO502", "Business Systems Analysis & Design", 5, 15, "--", "Analysing businesses and designing solution.", 1);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP503", "Introduction to Networks (Cisco 1)", 5, 15, "--", "Introduction to networking.", 2);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP504", "Operating Systems & systems Support", 5, 15, "--", "Support for OS.", 2);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO503", "Database Principles", 5, 15, "--", "Principles for Databases.", 2);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO504", "Technical Support", 5, 15, "--", "Support and Help desk.", 2);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP601", "Object Oriented Programming", 6, 15, "COMP502", "OBJ Best Practices.", 3);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO601", "Data-modelling and SQL", 6, 15, "INFO503", "Data modelling theory and implementation.", 3);
        dbHandler.insertModule(module);
        module = new Course("software", "MATH601", "Mathematics for IT", 6, 15, "--", "Math for IT.", 3);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP602", "Web Development", 6, 15, "COMP502, INFO502", "Website Development.", 3);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO602", "Business, Interpersonal Communications & Technical Writing", 6, 15, "--", "Business Comms and Practices.", 4);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP603", "The Web Environment", 6, 15, "COMP602", "The Website Environment.", 4);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP605", "Data Structures and Algorithms", 6, 15, "COMP601, MATH601", "Big O Notation.", 4);
        dbHandler.insertModule(module);
        module = new Course("software", "MATH602", "Mathematics for programming", 6, 15, "MATH601", "Math for Programming.", 4);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO701", "Project Management (Prince 2)", 7, 15, "INFO502", "Project management.", 5);
        dbHandler.insertModule(module);
        module = new Course("software", "BIZM701", "Business Essentials for IT Professionals", 7, 15, "INFO602", "Essentials for IT pros.", 5);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO703", "Big Data and Analytics", 7, 15, "COMP605, MATH602", "Big data analytics.", 5);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP706", "Games Development", 7, 15, "--", "Developing games for multi platform.", 5);
        dbHandler.insertModule(module);
        module = new Course("software", "INFO704", "Data-Warehousing and Business Intelligence", 7, 15, "--", "Data-warehousing and BI.", 6);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP707", "Principles of Software Testing", 7, 15, "COMP605", "Software testing principles.", 6);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP709", "Mobile Apps Development", 7, 15, "COMP601, COMP605, MATH602", "Mobile applications dev.", 6);
        dbHandler.insertModule(module);
        module = new Course("software", "COMP708", "Software Engineering Project", 7, 15, "COMP605, MATH602", "Project/Internship.", 6);
        dbHandler.insertModule(module);

        // setup viewpager
        screenPager = findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);

        // setup tablelayout with viewpager
        tabIndicator.setupWithViewPager(screenPager);


        // tablayout add changes listener
        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if(tab.getPosition() == mList.size()-1){

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }


}
