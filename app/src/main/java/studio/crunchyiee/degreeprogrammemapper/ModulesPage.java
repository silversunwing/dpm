package studio.crunchyiee.degreeprogrammemapper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class ModulesPage extends AppCompatActivity {

    //Get reference to GridView objects
    GridView gv1;
    GridView gv2;
    GridView gv3;
    GridView gv4;
    GridView gv5;
    GridView gv6;

    //Create DBHandler object
    DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modules_page);

        //Connect to database
        dbHandler = new DBHandler(this, null, null, 1);

        //Find GridView items
        gv1 = (GridView) findViewById(R.id.semester1Gv);
        gv2 = (GridView) findViewById(R.id.semester2Gv);
        gv3 = (GridView) findViewById(R.id.semester3Gv);
        gv4 = (GridView) findViewById(R.id.semester4Gv);
        gv5 = (GridView) findViewById(R.id.semester5Gv);
        gv6 = (GridView) findViewById(R.id.semester6Gv);

        //Get Modules based on semester
        String[] semester1Codes = dbHandler.getModuleSemester("software", 1);
        String[] semester2Codes = dbHandler.getModuleSemester("software", 2);
        String[] semester3Codes = dbHandler.getModuleSemester("software", 3);
        String[] semester4Codes = dbHandler.getModuleSemester("software", 4);
        String[] semester5Codes = dbHandler.getModuleSemester("software", 5);
        String[] semester6Codes = dbHandler.getModuleSemester("software", 6);

        //Set Card based on semester
        //Semester 1
        ArrayAdapter<String> gridViewArrayAdapter = new ModulesAdapter(this, semester1Codes);
        gv1.setAdapter(gridViewArrayAdapter);
        //Semester 2
        gridViewArrayAdapter = new ModulesAdapter(this, semester2Codes);
        gv2.setAdapter(gridViewArrayAdapter);
        //Semester 3
        gridViewArrayAdapter = new ModulesAdapter(this, semester3Codes);
        gv3.setAdapter(gridViewArrayAdapter);
        //Semester 4
        gridViewArrayAdapter = new ModulesAdapter(this, semester4Codes);
        gv4.setAdapter(gridViewArrayAdapter);
        //Semester 5
        gridViewArrayAdapter = new ModulesAdapter(this, semester5Codes);
        gv5.setAdapter(gridViewArrayAdapter);
        //Semester 6
        gridViewArrayAdapter = new ModulesAdapter(this, semester6Codes);
        gv6.setAdapter(gridViewArrayAdapter);
    }
}
